/**
 * @ngdoc overview
 * @name includes
 * @description: Included files
 *
 * @author: Haiping Lu
 */
define([
    'static/module',
    'static/model/StaticModel'
], function(){
    'use strict';
});