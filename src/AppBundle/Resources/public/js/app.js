'use strict';

/**
 * @ngdoc overview
 * @name app
 * @description: Main module of the application
 *
 * @author: Haiping Lu
 */

define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-animate',
    'angular-sanitize',
    'angular-bootstrap',
    'angular-chart',
], function (ng, couchPotato) {

    var app = ng.module('app', [
        'ngSanitize',
        'ngAnimate',
        'scs.couch-potato',
        'ui.router',
        'ui.bootstrap',
        'chart.js',
        // App
        'app.static'
    ]);
    
    couchPotato.configureApp(app);

    app.config(['$interpolateProvider', function($interpolateProvider) {

        $interpolateProvider.startSymbol('[[');
        $interpolateProvider.endSymbol(']]');
    }]);

    app.config(['$provide', '$httpProvider', function($provide, $httpProvider){

        $provide.factory('ErrorHttpInterceptor', function($q) {

            var errorCounter = 0;

            function notifyError(rejection){
                //Handle Rejection
            }

            return {
                requestError: function(rejection) {

                    notifyError(rejection);

                    return $q.reject(rejection);
                },

                responseError: function(rejection) {

                    notifyError(rejection);

                    return $q.reject(rejection);
                }
            }
        })

        $httpProvider.interceptors.push('ErrorHttpInterceptor');
    }])

    .config(['$tooltipProvider', function($tooltipProvider){

        $tooltipProvider.options({animation: false});
    }]);
    
    app.run(function ($couchPotato, $rootScope, $state, $stateParams, $location, $window) {
        app.lazy = $couchPotato,
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        //global variable used in js files or template
        $rootScope.appConfig = {
            apiUrl: angular.element('body').data('base-url') + '/api/',
        };
        
        var history = [];
        
        $rootScope.$on('$locationChangeStart', function(){
            history.push($location.$$path);
        });
        
        $rootScope.goBack = function() {
            var prevUrl = history.length > 1 ? history.splice(-2)[0] : '/';
            $location.path(prevUrl);
            history = [];
        };
        
    }).filter('propsFilter', function () {
        return function(items, props) {
            var out = [];

            if (angular.isArray(items)) {
                items.forEach(function(item) {
                    var itemMatches = false;

                    var keys = Object.keys(props);
                    for (var i = 0; i < keys.length; i++) {
                        var prop = keys[i];
                        var text = props[prop].toLowerCase();
                        if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                            itemMatches = true;
                            break;
                        }
                    }

                    if (itemMatches) {
                        out.push(item);
                    }
                });
            } else {
                // Let the output be the input untouched
                out = items;
            }

            return out;
        };
    }).filter('chunk', function(){
        return function(input, size) {
            if (angular.isArray(input)) {
                var chunk = [];
                
                for(var i = 0; i < input.length; i += size) {
                    chunk.push(input.slice(i, i + size));
                }
                return chunk;
            }
            
            return input;
        }
    }).filter("trustUrl", ['$sce', function ($sce) {
        return function (recordingUrl) {
            return $sce.trustAsResourceUrl(recordingUrl);
        };
    }]);

    return app;
});
