/**
 * @ngdoc overview
 * @name Static module
 * @description: Route definition
 *
 * @author: Haiping Lu
 */
define([
    'angular',
    'angular-couch-potato',
    'angular-ui-router',
    'angular-resource',
    'angular-bootstrap'
], function (ng, couchPotato) {

    "use strict";

    var module = ng.module('app.static', [
        'ui.router', 
        'ngResource',
        'ui.bootstrap'
    ]);
    
    couchPotato.configureApp(module);
    
    module.config(function ($stateProvider, $couchPotatoProvider, $urlRouterProvider) {

        $stateProvider
            // scope for server list
            .state('app', {
                url: '/',
                views: {
                    root: {
                        templateUrl: 'tmpl/static',
                        controller: 'StaticCtrl',
                        resolve: {
                            deps: $couchPotatoProvider.resolveDependencies([
                                'static/controller/StaticCtrl'
                            ]),
                            servers: ['StaticModel', function(StaticModel){
                                return StaticModel.getServers();
                            }]
                        }
                    }
                },
                data: {
                    // Additional data here
                }
            });

        $urlRouterProvider.otherwise('/');
    });
    
    module.run(function($couchPotato){
        
        module.lazy = $couchPotato;
    });
    
    return module;

});
