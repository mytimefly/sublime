/**
 * @ngdoc overview
 * @name StaticModel
 * @description: Model Layer
 *
 * @author: Haiping Lu
 */
define(['static/module'], function(module){
    
    "use strict";
    
    return module.registerFactory('StaticModel', ['$resource', '$rootScope', function($resource, $rootScope){
        
        var baseURL = $rootScope.appConfig.apiUrl + 'server';
        
        var StaticModel = $resource(
            baseURL,
            {Id: '@Id'},
            {
                getServers: {
                    url: baseURL,
                    method: 'GET',
                    isArray: false
                },
                getServerData: {
                    url: baseURL + '/:Id',
                    method: 'GET',
                    isArray: false
                }
            }
        );
        
        return StaticModel;
    }]);
});