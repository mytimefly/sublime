/**
 * @ngdoc overview
 * @name StaticCtrl
 * @description: Controller
 *
 * @author: Haiping Lu
 */
define(['static/module'], function (module) {
    
    module.registerController('StaticCtrl', [
        '$scope', 
        '$log',
        '$timeout',
        'servers',
        'StaticModel',
        function(
            $scope,
            $log,
            $timeout,
            servers,
            StaticModel
        ){
            
        $scope.ctrl = 'StaticCtrl';
        
        $scope.points = [
            {id: 0, name: 'All points'},
            {id: 10, name: '10 points per page'},
            {id: 20, name: '20 points per page'},
            {id: 30, name: '30 points per page'},
            {id: 40, name: '40 points per page'},
            {id: 50, name: '50 points per page'},
            {id: 60, name: '60 points per page'},
            {id: 70, name: '70 points per page'},
            {id: 80, name: '80 points per page'},
            {id: 90, name: '90 points per page'}
        ];
        
        $scope.offset = 0;
        
        $scope.pointPerPage = 30;
        
        servers.$promise.then(function(data){
            $scope.servers = data.rows;
        });
        
        $scope.updateChart = function(data, labels) {
            $timeout(function() {
                $scope.labels = labels;
                $scope.data = data;
            });
        };
        
        $scope.initChart = function(data){
    
            $scope.labels = [];
            var newLabels = [];
            var newData = [];
            
            if (angular.isDefined(data)) {
                $scope.totalData = data.value;
                $scope.summary = data.summary;
            }
            
            if ($scope.pointPerPage) {
                newData = [$scope.totalData.slice($scope.offset, $scope.offset + $scope.pointPerPage)];
            }
            else {
                newData = [ $scope.totalData ];
            }
            
            for (var i = 0; i < newData[0].length; i++) {
                newLabels[i] = '';
            }
            
            $scope.updateChart(newData, newLabels);

        };

        $scope.prev = function() {
            $scope.offset = $scope.offset - $scope.pointPerPage;
            $scope.initChart();
        };
        
        $scope.next = function() {
            $scope.offset = $scope.offset + $scope.pointPerPage;
            $scope.initChart();
        };

        $scope.$watch('server', function(newValue, oldValue){
            
            if(newValue) {

                StaticModel.getServerData({Id: newValue}).$promise.then(function(data){
                    $scope.offset = 0;
                    $scope.initChart(data.data);
                });
            }
        });
        
        $scope.$watch('pointPerPage', function(newValue, oldValue){
            
            if(angular.isDefined(newValue) && angular.isDefined($scope.totalData)) {
                $scope.offset = 0;
                $scope.initChart();
            }
        });
            
    }]);
});
    