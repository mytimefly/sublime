/**
 * @name: rconfig
 * @description: Define required js path
 *
 * @author: Haiping Lu
 */
var require = {

    waitSeconds: 0,
    
    paths: {
        'jquery': [
            '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min'
        ],
        'jquery-ui': '//ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min',
        'bootstrap': [
            '//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min'
        ],
        'angular': [
          '//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular.min'
        ],
        'angular-resource': '//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-resource.min',
        'angular-sanitize': '//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-sanitize.min',
        'angular-animate': '//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-animate.min',
        'angular-cookies': '//ajax.googleapis.com/ajax/libs/angularjs/1.5.7/angular-cookies.min',
        'angular-ui-router': 'library/angular/ui-router/0.2.14/angular-ui-router.min',
        'angular-couch-potato': 'library/angular/couch-potato/angular-couch-potato',
        'angular-bootstrap': 'library/angular/ui-bootstrap/0.14.3/ui-bootstrap-tpls.min',
        'domReady': 'library/domready/domReady',
        'chart': 'library/chart/Chart.min',
        'angular-chart': 'library/angular/chart/angular-chart.min',
        'modules-include': 'include',
        'app': 'app'
    },
    // angular does not support AMD out of the box, put it in a shim
    shim: {
        'jquery-ui': { deps: ['jquery']},
        'angular': {exports: 'angular', deps: ['jquery']},
        'angular-resource': {deps: ['angular']},
        'angular-animate': {deps: ['angular']},
        'angular-cookies': {deps: ['angular']},
        'angular-sanitize': {deps: ['angular']},
        'angular-bootstrap': {deps: ['angular']},
        'angular-ui-router': {deps: ['angular']},
        'angular-chart': {deps: ['angular', 'chart']},
        'angular-couch-potato': {deps: ['angular']},
        'bootstrap': {deps: ['jquery']},
        'modules-includes': { deps: ['angular']},
    },
    priority: [
        "jquery",
        "boostrap",
        "angular"
    ]
};