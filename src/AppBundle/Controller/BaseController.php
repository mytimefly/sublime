<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

class BaseController extends Controller
{
    public function getEntityManager()
    {
        return $this->getDoctrine()->getManager();
    }
    
    public function getRepository($entity)
    {
        $entity = ucfirst(ltrim($entity, ':'));
        
        return $this->getEntityManager()->getRepository(Model\Model::ENTITY_BUNDLE . ':' . $entity);
    }
    
    public function getJsonResponse($data)
    {
        return new JsonResponse($data, Response::HTTP_OK);
    }
    
}