<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class IndexController extends BaseController
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        return $this->render('index/index.html.twig', []);
    }
    
    /**
     * @Route("/tmpl/{tmpl}", name="template", requirements={"tmpl"="[a-zA-Z_\-]+"})
     */ 
    public function templateAction(Request $request, $tmpl)
    {
        return $this->render('index/tmpl/' . strtolower($tmpl) . '.html.twig', []);
    }
}