<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use AppBundle\Entity;

class ApiController extends BaseController
{
    /**
     * @Route("/api/server", name="server_list")
     */
    public function listAction(Request $request)
    {
        $servers = $this->get('server.model')->getServerList();
        
        return $this->getJsonResponse(array('rows' => $servers));
    }
    
    /**
     * @Route("/api/server/{id}", name="server_data", requirements={"id"="\d+"})
     */ 
    public function dataAction(Request $request, Entity\Server $server)
    {
        $data = $this->get('server.model')->getServerData($server);
        
        return $this->getJsonResponse(array('data' => $data));
    }
    
}