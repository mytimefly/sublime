<?php

namespace AppBundle\Repository;

/**
 * ServerDataRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ServerDataRepository extends \Doctrine\ORM\EntityRepository
{
    public function getServerData($serverId)
    {
        return $this->_em->createQueryBuilder()
                    ->select('sd.dataLabel AS data_label, sd.dataValue AS data_value')
                    ->from($this->_entityName, 'sd')
                    ->where('sd.serverId = :ServerId')
                    ->setParameter('ServerId', $serverId)
                    ->getQuery()
                    ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
    
    public function getDataCount($serverId)
    {
        return $this->_em->createQueryBuilder()
                    ->select('COUNT(sd.id)')
                    ->from($this->_entityName, 'sd')
                    ->where('sd.serverId = :ServerId')
                    ->setParameter('ServerId', $serverId)
                    ->getQuery()
                    ->getSingleScalarResult();
    }
    
    public function getDataValue($serverId)
    {
        $result = $this->_em->createQueryBuilder()
                    ->select('sd.dataValue')
                    ->from($this->_entityName, 'sd')
                    ->where('sd.serverId = :ServerId')
                    ->setParameter('ServerId', $serverId)
                    ->getQuery()
                    ->getScalarResult();
        return array_map('current', $result);
    }
    
    public function getSummary($serverId)
    {
        $result = $this->_em->createQueryBuilder()
                    ->select('MAX(sd.dataValue) AS max_value, MIN(sd.dataValue) AS min_value, AVG(sd.dataValue) AS avg_value')
                    ->from($this->_entityName, 'sd')
                    ->where('sd.serverId = :ServerId')
                    ->setParameter('ServerId', $serverId)
                    ->getQuery()
                    ->getScalarResult();
        return array_shift($result);
        
    }
}
