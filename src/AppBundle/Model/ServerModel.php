<?php

namespace AppBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity;

class ServerModel extends Model
{  
    const BATCH_SIZE = 50;
    
    public function getServerList()
    {
        $rows = array();
        
        if (!$servers = $this->getRepository('Server')->findAll()) {
            // save JSON data from Sublime API to local database at first time
            $data = $this->get('backend.process.service')->getServerList();
            
            if (count($data)) {
                $em = $this->getManager();
                
                foreach($data as $r) {
                    $server = new Entity\Server;
                    $server->setName($r['s_system']);
                    
                    $em->persist($server);
                }
                // physically save data to database
                $em->flush();
                $em->clear();
            }
            
            $servers = $this->getRepository('Server')->findAll();
        }
        
        foreach($servers as $s) {
            $rows[] = array('id' => $s->getId(), 'name' => $s->getName()); 
        }
        
        return $rows;
    }
    
    public function getServerData(Entity\Server $server)
    {
        $serverData = array();
        
        if (!$this->getRepository('ServerData')->getDataCount($server->getId())) {
            // save JSON data from Sublime API to local database at first time
            $data = $this->get('backend.process.service')->getServerData($server->getName());

            if (count($data)) {
                
                $em = $this->getManager();
                
                for($i = 0; $i < count($data); $i++) {
                    $serverData = new Entity\ServerData;
                    $serverData->setServer($server);
                    // we save label as datetime, instead of string, it is better to group data by day, week or month
                    $serverData->setDataLabel(new \DateTime($data[$i]['data_label']));
                    $serverData->setDataValue($data[$i]['data_value']);
                    $server->addDatum($serverData);
                }
                $em->persist($server);
                $em->flush(); // physically save data to database
            }
        }
        
        $value = $this->getRepository('ServerData')->getDataValue($server->getId());
        $summary = $this->getRepository('ServerData')->getSummary($server->getId());
        
        return compact('value', 'summary');

    }
}