<?php
namespace AppBundle\Model;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\Collection;
use AppBundle\Entity;

class Model
{
    const ENTITY_BUNDLE = 'AppBundle';
    
    protected $_container;
    
    public function __construct(Container $container)
    {
        $this->_container = $container;
    }
    
    public function getManager()
    {
        return $this->_container->get('doctrine')->getManager();
    }
    
    public function getRepository($name)
    {
        $name = ltrim($name, ':');
        
        return $this->getManager()->getRepository(self::ENTITY_BUNDLE . ':' . $name);
    }
    
    public function get($id)
    {
        return $this->_container->get($id);
    }
}