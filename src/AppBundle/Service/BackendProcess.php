<?php
namespace AppBundle\Service;

use Symfony\Component\DependencyInjection\Container;

class BackendProcess
{
    const SERVER_LIST_POINT = 'servers';
    const SERVER_DATA_POINT = 'iq/server/';
    
    protected $_container;
    
    
    public function __construct(Container $container) 
    {
        $this->_container = $container;
    }
    
    public function getServerList()
    {
        $url = $this->_container->getParameter('sublime_api_prefix') . self::SERVER_LIST_POINT;
        
        return $this->_callSublimeApi($url);
    }
    
    public function getServerData($server)
    {
        $url = $this->_container->getParameter('sublime_api_prefix') . self::SERVER_DATA_POINT . $server;
        
        return $this->_callSublimeApi($url);
    }
    
    protected function _callSublimeApi($url)
    {
        if(!function_exists("curl_init")) {
            throw new \Exception("cURL extension is not installed");
        }

        $curlOptions = array(
            CURLOPT_URL => $url,
            CURLOPT_HEADER => 0,
            CURLOPT_RETURNTRANSFER => TRUE,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_FOLLOWLOCATION => TRUE,
            CURLOPT_ENCODING => 'gzip,deflate',
        );

        $ch = curl_init();
        curl_setopt_array($ch, $curlOptions);
        $output = curl_exec($ch);
        curl_close($ch);

        return json_decode($output, true);
    }
}