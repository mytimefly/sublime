<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ServerData
 *
 * @ORM\Table(name="server_data")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ServerDataRepository")
 */
class ServerData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="server_id", type="integer")
     */
    private $serverId;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="data_label", type="datetime")
     */
    private $dataLabel;

    /**
     * @var int
     *
     * @ORM\Column(name="data_value", type="integer")
     */
    private $dataValue;

    /**
     * @ORM\ManyToOne(targetEntity="Server", inversedBy="data")
     * @ORM\JoinColumn(name="server_id", referencedColumnName="id", onDelete="CASCADE")
     **/
    private $server;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dataLabel
     *
     * @param \DateTime $dataLabel
     *
     * @return ServerData
     */
    public function setDataLabel($dataLabel)
    {
        $this->dataLabel = $dataLabel;

        return $this;
    }

    /**
     * Get dataLabel
     *
     * @return \DateTime
     */
    public function getDataLabel()
    {
        return $this->dataLabel;
    }

    /**
     * Set dataValue
     *
     * @param integer $dataValue
     *
     * @return ServerData
     */
    public function setDataValue($dataValue)
    {
        $this->dataValue = $dataValue;

        return $this;
    }

    /**
     * Get dataValue
     *
     * @return int
     */
    public function getDataValue()
    {
        return $this->dataValue;
    }

    /**
     * Set serverId
     *
     * @param integer $serverId
     *
     * @return ServerData
     */
    public function setServerId($serverId)
    {
        $this->serverId = $serverId;

        return $this;
    }

    /**
     * Get serverId
     *
     * @return integer
     */
    public function getServerId()
    {
        return $this->serverId;
    }

    /**
     * Set server
     *
     * @param \AppBundle\Entity\Server $server
     *
     * @return ServerData
     */
    public function setServer(\AppBundle\Entity\Server $server = null)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * Get server
     *
     * @return \AppBundle\Entity\Server
     */
    public function getServer()
    {
        return $this->server;
    }
}
