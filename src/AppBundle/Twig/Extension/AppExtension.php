<?php
namespace AppBundle\Twig\Extension;

use Symfony\Component\DependencyInjection\Container;


class AppExtension extends \Twig_Extension
{

    protected $_container;

    public function __construct(Container $container)
    {
        $this->_container = $container;
    }


    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(

        );
    }
}
