Code Structure
--------------
This test is developed based on Symfony 3.1. A demo site for this test is located at http://sublime.weddingnsave.com
 
 * Server side scripts
 
    * Using PHP and MySQL
 
    * Using twig as view template, view files are located at app/Resources/views
 
	* PHP Controller files are located at src/AppBundle/Controller

	* Entity files map PHP classes to DB tables at src/Entity
	
	* DB queries are located at src/Repository

	* Model files for business logic are located at src/Model
	
	* Service implementation to communicate with external Sublime APIs are located at src/Service
	
	* DB dump file is located at sql/sublime.sql

 * Client side scripts
    
	* Using Bootstrap 3.3.5 and Angular 1.5.7 from remote CDN and Googleapis site
	
	* All Less and js srouce code are located src/AppBundle/Resources/public
	
How it works
------------
All data from external Sublime APIs will be saved to local database at first time. After that, the data in local database is used to populate in front end.

There is a 'points per page' select box to show how much data are shown in line chart at one time.

 * All points - display all data together at one time
 
 * 10/20..80/90 points per page - display part of points at one time, and use backward and forward button to navigate it.

Install this test in local
--------------------------
 * Git clone https://mytimefly@bitbucket.org/mytimefly/sublime.git path/to/sublime-test-folder
 
 * Install Symfony Bundles via composer
 
 * setting database configuration as required
 
 * Run 'composer require symfony/assetic-bundle' to install AsseticBundle if there is an error at above step
 
 * Run 'composer require leafo/lessphp' to install leafo/lessphp if there is an error at above step
 
 * Run 'php bin/console assets:install --env=prod' under sublime-test-folder to expose js code to public folder
 
 * Run 'php bin/console doctrine:schema:update --force --env=prod' to setup database table
 
 * Run 'chmod 777 sublime-test-folder/var/cache'
 
 * Run 'chmod 777 sublime-test-folder/var/logs'



Symfony Standard Edition
========================

Welcome to the Symfony Standard Edition - a fully-functional Symfony
application that you can use as the skeleton for your new applications.

For details on how to download and get started with Symfony, see the
[Installation][1] chapter of the Symfony Documentation.

What's inside?
--------------

The Symfony Standard Edition is configured with the following defaults:

  * An AppBundle you can use to start coding;

  * Twig as the only configured template engine;

  * Doctrine ORM/DBAL;

  * Swiftmailer;

  * Annotations enabled for everything.

It comes pre-configured with the following bundles:

  * **FrameworkBundle** - The core Symfony framework bundle

  * [**SensioFrameworkExtraBundle**][6] - Adds several enhancements, including
    template and routing annotation capability

  * [**DoctrineBundle**][7] - Adds support for the Doctrine ORM

  * [**TwigBundle**][8] - Adds support for the Twig templating engine

  * [**SecurityBundle**][9] - Adds security by integrating Symfony's security
    component

  * [**SwiftmailerBundle**][10] - Adds support for Swiftmailer, a library for
    sending emails

  * [**MonologBundle**][11] - Adds support for Monolog, a logging library

  * **WebProfilerBundle** (in dev/test env) - Adds profiling functionality and
    the web debug toolbar

  * **SensioDistributionBundle** (in dev/test env) - Adds functionality for
    configuring and working with Symfony distributions

  * [**SensioGeneratorBundle**][13] (in dev/test env) - Adds code generation
    capabilities

  * **DebugBundle** (in dev/test env) - Adds Debug and VarDumper component
    integration

All libraries and bundles included in the Symfony Standard Edition are
released under the MIT or BSD license.

Enjoy!

[1]:  https://symfony.com/doc/3.0/book/installation.html
[6]:  https://symfony.com/doc/current/bundles/SensioFrameworkExtraBundle/index.html
[7]:  https://symfony.com/doc/3.0/book/doctrine.html
[8]:  https://symfony.com/doc/3.0/book/templating.html
[9]:  https://symfony.com/doc/3.0/book/security.html
[10]: https://symfony.com/doc/3.0/cookbook/email.html
[11]: https://symfony.com/doc/3.0/cookbook/logging/monolog.html
[13]: https://symfony.com/doc/3.0/bundles/SensioGeneratorBundle/index.html

